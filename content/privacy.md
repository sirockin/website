---
title: Privacy Policy
comments: false
---

This site collects no personal data directly.

We do use one third party service - [Disqus](https://disqus.com) - to collect and display comments on blog posts if and when you click the "Show Comments" button. They collect data according to their own policy and the options you select within their website.