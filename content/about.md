---
title: About me
comments: false
---

I'm Dave Sirockin: Software, electrical & electronics engineer, armchair activist, former free-flier who crashed too often, occasional sailor.

Originally from London, now living in France.
