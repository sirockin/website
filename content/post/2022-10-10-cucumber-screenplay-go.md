---
title: Acceptance testing using the Screenplay Pattern with Cucumber and Go
subtitle: It's deep
date: 2022-10-10
draft: true
tags: ["go", "screenplay", "BDD", "Cucumber", "Gherkin", "acceptance testing"]
---

## Introduction

This post was inspired by a new [draft chapter](https://github.com/quii/go-specs-greet) in Chris James' excellent [Learn Go With Tests](https://quii.gitbook.io/learn-go-with-tests/) book, which presents some key concepts and provides a walk-through of a well-designed structure for acceptance testing. The finished code provides good separation of concerns between domain model, implementations (such as `http` and `grpc`) and test specifications, and allows the same set of specifications to be used to separately test both the domain model and the different implementations.

The chapter also refers to [BDD](https://www.agilealliance.org/glossary/bdd/) techniques - in particular the [Screenplay Pattern](https://serenity-js.org/handbook/design/screenplay-pattern.html) - but does not include examples of these in the code. 

Having already practised BDD-style testing in go using a couple of frameworks as well as hand-rolled methods, I was curious as to how these techniques could be applied to Chris' structure. In particular the Screenplay Pattern seemed to add a very useful element to the BDD process, both in specifying the interactions and cleanly modelling these in testing code. The best worked examples I could find of the Screenplay pattern used  `javascript` frameworks - [serenity-js](https://serenity-js.org/handbook/design/screenplay-pattern.html) and [cucumber-js](https://cucumber-school.github.io/screenplay-example/). Since Cucumber also has a go implementation - [Godog](https://github.com/cucumber/godog) - I decided to try adapting the [cucumber-js](https://cucumber-school.github.io/screenplay-example/) example using [Godog](https://github.com/cucumber/godog) then restructure this to fit Chris' structure.

Another motivation for using [Cucumber](https://cucumber.io/) is that it offers an implementation-independent human-language aimed at non-technical humans ([Gherkin](https://cucumber.io/docs/guides/overview/#what-is-gherkin)) for defining specifications and I was curious about its usefulness.

## Prerequisites

This post assumes a reasonable understanding of the Go language, and does not delve as deeply into patterns and techniques as the two main sources, so I recommend reading these for insights into well-structured acceptance testing, BDD and the Screenplay pattern:
- [Learn Go with Tests - Scaling Acceptance Tests (and light intro to gRPC)](https://github.com/quii/go-specs-greet) 
- [Cucumber Example: Understanding Screenplay](https://cucumber-school.github.io/screenplay-example/)

## Getting Started

We use the official Cucumber `go` library - [Godog](https://github.com/cucumber/godog) to translate Gherkin specs and run and parse tests.

1. Install the `godog` binary: 
   ```bash
   go install github.com/cucumber/godog/cmd/godog@latest
   ```

2. Create a new project directory and CD into it:   
    ```bash
    mkdir cucumber-screenplay-go && cd cucumber-screenplay-go
    ```
3. Initialise a go module. (You may choose a different module name but will need to change import links as appropriate later on):
    ```bash
    go mod init github.com/sirockin/cucumber-screenplay-go
    ```

## Add Specifications

Test specifications in Cucumber are organised by `Features` each of which contain one or more `Scenarios`. Create a subdirectory, `features` and add the following files:

`./features/sign_up.feature`:
```gherkin
Feature: Sign up

  New accounts need to confirm their email to activate
  their account.

  Scenario: Successful sign-up
    Given Tanya has created an account
    When Tanya activates her account
    Then Tanya should be authenticated

  Scenario: Try to sign in without activating account
    Given Bob has created an account
    When Bob tries to sign in
    Then Bob should not be authenticated
    And Bob should see an error telling him to activate the account

```

`./features/create_project.feature`:
```gherkin
Feature: Sign up

  New accounts need to confirm their email to activate
  their account.

  Scenario: Successful sign-up
    Given Tanya has created an account
    When Tanya activates her account
    Then Tanya should be authenticated

  Scenario: Try to sign in without activating account
    Given Bob has created an account
    When Bob tries to sign in
    Then Bob should not be authenticated
    And Bob should see an error telling him to activate the account

```

These specifications are written in Cucumber's  ([Gherkin](https://cucumber.io/docs/guides/overview/#what-is-gherkin)) language. They represent the source of truth for our tests and will be used to generate test code Those who have used BDD before will recognise the `Given/When/Then` format. 

Even those who have never used a programming language before, should be able to understand the intent and after some cursory reading of the language's documentation, be able to write their own specifications. 

From an implementation perspective, the strings following the `Given`/`Then`/`Then`/`And` keywords will be mapped to test functions in our code. The Text between the `Feature` line and the first `Scenario` is treated as a comment.

## Generate Test Code

From the project root, run `godog run`. The console output shows that `godog` has  parsed the files and listed the resulting scenarios and steps, summarising them as 'undefined'. It then lists some code snippets which can be used to define the missing steps:

```bash
$ godog run
Feature: Create project
  Users can create projects, only visible to themselves

  Scenario: Create one project      # features/create_project.feature:5
    Given Sue has signed up
    When Sue creates a project
    Then Sue should see the project

  Scenario: Try to see someone else's project # features/create_project.feature:10
    Given Sue has signed up
    And Bob has signed up
    When Sue creates a project
    Then Bob should not see any projects

Feature: Sign up
  New accounts need to confirm their email to activate
  their account.

  Scenario: Successful sign-up         # features/sign_up.feature:6
    Given Tanya has created an account
    When Tanya activates her account
    Then Tanya should be authenticated

  Scenario: Try to sign in without activating account               # features/sign_up.feature:11
    Given Bob has created an account
    When Bob tries to sign in
    Then Bob should not be authenticated
    And Bob should see an error telling him to activate the account

4 scenarios (4 undefined)
14 steps (14 undefined)
796.9µs

You can implement step definitions for undefined steps with these snippets:

func bobHasCreatedAnAccount() error {
        return godog.ErrPending
}

func bobHasSignedUp() error {
        return godog.ErrPending
}

func bobShouldNotBeAuthenticated() error {
        return godog.ErrPending
}

func bobShouldNotSeeAnyProjects() error {
        return godog.ErrPending
}

func bobShouldSeeAnErrorTellingHimToActivateTheAccount() error {
        return godog.ErrPending
}

func bobTriesToSignIn() error {
        return godog.ErrPending
}

func sueCreatesAProject() error {
        return godog.ErrPending
}

func sueHasSignedUp() error {
        return godog.ErrPending
}

func sueShouldSeeTheProject() error {
        return godog.ErrPending
}

func tanyaActivatesHerAccount() error {
        return godog.ErrPending
}

func tanyaHasCreatedAnAccount() error {
        return godog.ErrPending
}

func tanyaShouldBeAuthenticated() error {
        return godog.ErrPending
}

func InitializeScenario(ctx *godog.ScenarioContext) {
        ctx.Step(`^Bob has created an account$`, bobHasCreatedAnAccount)
        ctx.Step(`^Bob has signed up$`, bobHasSignedUp)
        ctx.Step(`^Bob should not be authenticated$`, bobShouldNotBeAuthenticated)
        ctx.Step(`^Bob should not see any projects$`, bobShouldNotSeeAnyProjects)
        ctx.Step(`^Bob should see an error telling him to activate the account$`, bobShouldSeeAnErrorTellingHimToActivateTheAccount)
        ctx.Step(`^Bob tries to sign in$`, bobTriesToSignIn)
        ctx.Step(`^Sue creates a project$`, sueCreatesAProject)
        ctx.Step(`^Sue has signed up$`, sueHasSignedUp)
        ctx.Step(`^Sue should see the project$`, sueShouldSeeTheProject)
        ctx.Step(`^Tanya activates her account$`, tanyaActivatesHerAccount)
        ctx.Step(`^Tanya has created an account$`, tanyaHasCreatedAnAccount)
        ctx.Step(`^Tanya should be authenticated$`, tanyaShouldBeAuthenticated)
}
```

We'll copy the code snippets from the console code into two separate files, for now in the project root, each with package `main_test` and importing `github.com/cucumber/godog` into each:
- the last function (`InitializeScenario()`) into `./suite_test.go` 
- the rest into `./steps_test.go`. 

`./suite_test.go`:
```go
package main_test

import "github.com/cucumber/godog"

func InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Step(`^Bob has created an account$`, bobHasCreatedAnAccount)
	ctx.Step(`^Bob has signed up$`, bobHasSignedUp)
	ctx.Step(`^Bob should not be authenticated$`, bobShouldNotBeAuthenticated)
	ctx.Step(`^Bob should not see any projects$`, bobShouldNotSeeAnyProjects)
	ctx.Step(`^Bob should see an error telling him to activate the account$`, bobShouldSeeAnErrorTellingHimToActivateTheAccount)
	ctx.Step(`^Bob tries to sign in$`, bobTriesToSignIn)
	ctx.Step(`^Sue creates a project$`, sueCreatesAProject)
	ctx.Step(`^Sue has signed up$`, sueHasSignedUp)
	ctx.Step(`^Sue should see the project$`, sueShouldSeeTheProject)
	ctx.Step(`^Tanya activates her account$`, tanyaActivatesHerAccount)
	ctx.Step(`^Tanya has created an account$`, tanyaHasCreatedAnAccount)
	ctx.Step(`^Tanya should be authenticated$`, tanyaShouldBeAuthenticated)
}
```
`./steps_test.go`:
```go
package main_test

import "github.com/cucumber/godog"

func bobHasCreatedAnAccount() error {
	return godog.ErrPending
}

func bobHasSignedUp() error {
	return godog.ErrPending
}

func bobShouldNotBeAuthenticated() error {
	return godog.ErrPending
}

func bobShouldNotSeeAnyProjects() error {
	return godog.ErrPending
}

func bobShouldSeeAnErrorTellingHimToActivateTheAccount() error {
	return godog.ErrPending
}

func bobTriesToSignIn() error {
	return godog.ErrPending
}

func sueCreatesAProject() error {
	return godog.ErrPending
}

func sueHasSignedUp() error {
	return godog.ErrPending
}

func sueShouldSeeTheProject() error {
	return godog.ErrPending
}

func tanyaActivatesHerAccount() error {
	return godog.ErrPending
}

func tanyaHasCreatedAnAccount() error {
	return godog.ErrPending
}

func tanyaShouldBeAuthenticated() error {
	return godog.ErrPending
}
```


Examining these files side by side, we can see:
-  `InitializeScenario()` calls `ctx.Step()` to register each of the strings following `Given/When/Then/And` keywords listed in the features file, to a `go` function listed in `./features/steps.go`
-  each of the step functions returns an error, currently `godog.ErrPending`

Run `go mod tidy` to add the newly imported `Godog` dependency. Running `godog run` now shows that each of the steps defined in the two `.feature` files is now implemented but the scenarios and steps are still shown as pending since the step functions all return  `godog.ErrPending`.

Changing the return value of all the steps in a scenario to `nil` will allow it to pass; returning any error other than `godog.ErrPending` will cause it to fail. Try it!

This approach of passing errors out of tests rather than calling methods on `testing.T` is unusual  in `go`, makes it difficult to use standard assertion libraries and could be considered a drawback to using `godog`.

## Parametrise Steps

Looking more closely at the step definitions we can see:
- Each of the steps refers to a person (Tanya, Sue or Bob) carrying out a task or finding something out. 
- Some of these steps (such as "has signed up") are repeated for more than one person.
- The strings in each step of `InitializeScenario()` are in the form of regular expressions

It is likely that each of these steps will be the same for each person so it makes sense to keep our tests DRY by parametrising them.

We can include capture groups in each of the regular expressions and `Godog` will then map these, in order, to basic atomic `go` types in the test implementation expressions. (Gherkin also has its own simpler syntax for expressing parameters in its feature strings as well as a means to create custom parameter types but neither of these features is supported by `Godog`.)

To start:

In `suite_test.go`:
1. Change the first line in `InitializeScenario()` to read:   
    ```go
	ctx.Step(`^(Bob|Sue|Tanya) has created an account$`, personHasCreatedAnAccount)
    ```
1. Remove the line:
   ```go
	ctx.Step(`^Tanya has created an account$`, tanyaHasCreatedAnAccount)
   ```
In `steps_test.go`:
1. Replace the function `bobHasCreatedAnAccount()` with:
   ```go
    func personHasCreatedAnAccount(name string) error {
        return fmt.Errorf("personHasCreatedAnAccount(%s) can't yet create an account", name)
    }
    ````
2. Run `godog run` again. This time, the two scenarios which include "create an account" will fail (as the new function is returning an error) with the name parameter displayed as expected in the error message:

    ```bash
    --- Failed steps:

    Scenario: Successful sign-up # features/sign_up.feature:6
        Given Tanya has created an account # features/sign_up.feature:7
        Error: personHasCreatedAnAccount(Tanya) can't yet create an account

    Scenario: Try to sign in without activating account # features/sign_up.feature:11
        Given Bob has created an account # features/sign_up.feature:12
        Error: personHasCreatedAnAccount(Bob) can't yet create an account

    ```

We can now edit all the same functions in the same way (but make them all return `err.Pending` for now). In doing so we need to remember to replace gendered pronouns such as "him" and "her" with suitable capture groups.

`./suite_test.go`:
```go
package main_test

import "github.com/cucumber/godog"

func InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Step(`^(Bob|Sue|Tanya) has created an account$`, personHasCreatedAnAccount)
	ctx.Step(`^(Bob|Sue|Tanya) has signed up$`, personHasSignedUp)
	ctx.Step(`^(Bob|Sue|Tanya) should not be authenticated$`, personShouldNotBeAuthenticated)
	ctx.Step(`^(Bob|Sue|Tanya) should not see any projects$`, personShouldNotSeeAnyProjects)
	ctx.Step(`^(Bob|Sue|Tanya) should see an error telling (him|her) to activate the account$`, personShouldSeeAnErrorTellingThemToActivateTheAccount)
	ctx.Step(`^(Bob|Sue|Tanya) tries to sign in$`, personTriesToSignIn)
	ctx.Step(`^(Bob|Sue|Tanya) creates a project$`, personCreatesAProject)
	ctx.Step(`^(Bob|Sue|Tanya) should see the project$`, personShouldSeeTheProject)
	ctx.Step(`^(Bob|Sue|Tanya) activates (his|her) account$`, personActivatesTheirAccount)
	ctx.Step(`^(Bob|Sue|Tanya) should be authenticated$`, personShouldBeAuthenticated)
}
```
`./steps_test.go`:
```go
package main_test

import (
	"github.com/cucumber/godog"
)

func personHasCreatedAnAccount(name string) error {
	return godog.ErrPending
}

func personHasSignedUp(name string) error {
	return godog.ErrPending
}

func personShouldNotBeAuthenticated(name string) error {
	return godog.ErrPending
}

func personShouldNotSeeAnyProjects(name string) error {
	return godog.ErrPending
}

func personShouldSeeAnErrorTellingThemToActivateTheAccount(name string) error {
	return godog.ErrPending
}

func personTriesToSignIn(name string) error {
	return godog.ErrPending
}

func personCreatesAProject(name string) error {
	return godog.ErrPending
}

func personShouldSeeTheProject(name string) error {
	return godog.ErrPending
}

func personActivatesTheirAccount(name string) error {
	return godog.ErrPending
}

func personShouldBeAuthenticated(name string) error {
	return godog.ErrPending
}
```

## Run Tests From Go

Currently we have to run our tests directly from the `Godog` CLI. This isn't always convenient, especially for debugging.

We can solve this by adding the following function with standard `go` testing signature to  `suite_test.go`:
```go
func TestFeatures(t *testing.T) {
	suite := godog.TestSuite{
	  ScenarioInitializer: InitializeScenario,
	  Options: &godog.Options{
		Format:   "pretty",
		Paths:    []string{"features"},
		TestingT: t, // Testing instance that will run subtests.
	  },
	}
  
	if suite.Run() != 0 {
	  t.Fatal("non-zero status returned, failed to run feature tests")
	}
  }
```

Running `go test` will now call this function and run our scenarios as go sub-tests. 

We now have a nice DRY boilerplate for our tests and could simply go ahead now and fill in the steps of each scenario with initially-failing methods, then implement one by one and watch them turn green.








