## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

The cupper theme is installed as a submodule so cloning normally will lead to its folder being empty. So either 
- use the `--recursive` flag when cloning or
- afterwards run `git submodule update --init`

To work locally with this project, :

1. [Install](https://gohugo.io/getting-started/installing/) Hugo
2. Preview project: `hugo server` then navigate to http://localhost:1313
3. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][https://gohugo.io/documentation/].

## Draft Content

Adding the `draft: true` tag to an article excludes it from the finished build. To preview draft content run `hugo --buildDrafts server`

## Syntax Colouring

Is provided in the Cupper theme by prism.js and overridden to support the languages we want in `./themes/my-overrides`.

To updated the supported languages and features:
1.  go to [this link](https://prismjs.com/download.html#themes=prism&languages=markup+css+clike+javascript+bash+csv+docker+gherkin+go+go-module+handlebars+hcl+jq+json+json5+makefile+markup-templating+plant-uml+sass+scss+sql+typescript+yaml)
1.  choose the required features
1.  download the js and css files and replace the existing ones in `./themes/my-overrides`
1.  save the new prism link back to the link in Step 1

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
